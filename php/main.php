<?php
/* Solo funciona para jpg, se debe modificar si se usa otro formato*/ 
        function createthumb($name,$filename,$new_w,$new_h){        
          /*$system = explode('.',$name);
          if (preg_match('/jpg|jpeg/',$system[1])){
            $src_img=imagecreatefromjpeg($name);
          }
          if (preg_match('/png/',$system[1])){
            $src_img=imagecreatefrompng($name);
          }*/
          $src_img=imagecreatefrompng($name);
          $old_x=imageSX($src_img);
          $old_y=imageSY($src_img);
          if ($old_x > $old_y) {
            $thumb_w=$new_w;
            $thumb_h=$old_y*($new_h/$old_x);
          }
          if ($old_x < $old_y) {
            $thumb_w=$old_x*($new_w/$old_y);
            $thumb_h=$new_h;
          }
          if ($old_x == $old_y) {
            $thumb_w=$new_w;
            $thumb_h=$new_h;
          }
          $dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
          imagealphablending($dst_img, FALSE);
          imagesavealpha($dst_img, TRUE);
          imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
          //imageCopyResized($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
          /*if (preg_match("/png/",$system[1]))
          {
            imagepng($dst_img,$filename); 
          } 
          else {
            imagejpeg($dst_img,$filename); 
          }*/
          imagepng($dst_img,$filename); 
          imagedestroy($dst_img); 
          imagedestroy($src_img);
        
}

function saveImage($datos)
    	 {
               
          // requires php5
          /*
          define('UPLOAD_DIR',$UPLOAD_DIR);
          $img = $datos['img'];
          $img = str_replace('data:image/jpg;base64,', '', $img);
          $img = str_replace(' ', '+', $img);
          $data = base64_decode($img);
          $file = UPLOAD_DIR . uniqid() . '.jpg';
          $success = file_put_contents($file, $data);*/          
          $UPLOAD_DIR ='../u/';
          $UPLOAD_THUMB_DIR ='../u/thumbs/';
          $data = $datos['img'];
          $uuid = md5(uniqid());
          $nombreImagen = $uuid . '.png';
          $file = $UPLOAD_DIR . $nombreImagen;
          $thumb = $UPLOAD_THUMB_DIR . $nombreImagen;
          $uri = explode(",",$data);

          // save to file
          $success = file_put_contents($file, base64_decode($uri[1]));
          createthumb($file,$thumb,116,116);
          $ans['error'] = $success?false:true;
          $ans['details'] = $success;
          //$ans['raw'] = $uri[1];

          if($success)
          {
            $info['image'][0] = $nombreImagen;
            //$info['imagen'][1] = $thumb;
            $ans['data'] = $info;
          }
          
          echo json_encode($ans);
        
}

?>