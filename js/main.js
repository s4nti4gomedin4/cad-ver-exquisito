(function(){
	
	var name;
	var lastname;

	var head_arrow_left_btn;
	var body_arrow_left_btn;
	var foot_arrow_left_btn;

	var head_arrow_right_btn;
	var body_arrow_right_btn;
	var foot_arrow_right_btn;


	var	person_head;
	var person_body;
	var person_foot;

	var random_btn;
	var save_btn;
	var download_btn;

	var person_container;
	var person_colum;


	var headCount = 0;
	var bodyCount = 0;
	var footCount = 0;
	var bgCount = 0;
	

	
	var maxHeadImages = 25;
	var maxBodyImages = 5;
	var maxFootImages = 5;
	var maxBgImages = 3;
	var canvasPerson;

	var galleryMaxImages=9;
	var galleryImageIndex=1;
	var nameIndexGallery="ImageGallery";
	var shownImage;

	var names = [];
	var lastnames = [];

	window.onload = function ()
	{
		head_arrow_left_btn = $("#head_arrow_left_btn");
		body_arrow_left_btn = $("#body_arrow_left_btn");
		foot_arrow_left_btn = $("#foot_arrow_left_btn");

		head_arrow_right_btn = $("#head_arrow_right_btn");
		body_arrow_right_btn = $("#body_arrow_right_btn");
		foot_arrow_right_btn = $("#foot_arrow_right_btn");

		person_head = $(".person_head");
		person_body = $(".person_body");
		person_foot = $(".person_foot");

		person_container=$(".person_container");
		person_colum=$("#person-colum");



		random_btn = $("#random_btn");
		save_btn = $("#save_btn");
		download_btn = $("#download_btn");
		change_bg_btn = $("#background_btn");
		shownImage= $("#shownImage");

		name = document.getElementById("name"); 
		lastname = document.getElementById("lastname");

		head_arrow_left_btn.click(function(){
			changeHead({dir: "left"});
		});
		body_arrow_left_btn.click(function(){
			changeBody({dir: "left"});
		});
		foot_arrow_left_btn.click(function(){
			changeFoot({dir: "left"});
		});

		head_arrow_right_btn.click(function(){
			changeHead({dir: "right"});
		});
		body_arrow_right_btn.click(function(){
			changeBody({dir: "right"});
		});
		foot_arrow_right_btn.click(function(){
			changeFoot({dir: "right"});
		});

		random_btn.click(function(){
			randomPerson();
		});

		change_bg_btn.click(function(){
			changeBg();
		});

		loadImagesLocalStorage();
		fillNames();

		randomPerson();
		randomBg();
		setShareImage();
		setDownloadImage();
		setSaveImage();

	}
	function loadImagesLocalStorage(){
		
		
        gallery_img1=$("#gallery_img1");    
        gallery_img2=$("#gallery_img2"); 
        gallery_img3=$("#gallery_img3"); 
        gallery_img4=$("#gallery_img4"); 
        gallery_img5=$("#gallery_img5"); 
        gallery_img6=$("#gallery_img6"); 
        gallery_img7=$("#gallery_img7"); 
        gallery_img8=$("#gallery_img8"); 
        gallery_img9=$("#gallery_img9");     
		 
		setImageGallery(gallery_img1,1);
		setImageGallery(gallery_img2,2);
		setImageGallery(gallery_img3,3);
		setImageGallery(gallery_img4,4);
		setImageGallery(gallery_img5,5);
		setImageGallery(gallery_img6,6);
		setImageGallery(gallery_img7,7);
		setImageGallery(gallery_img8,8);
		setImageGallery(gallery_img9,9);	




	}

	function setImageGallery(item,index){
		data=getImageLocalStorage(index);
		if(data!=null){
		image = new Image();
        image.src = getImageLocalStorage(index);
       
        image.width=item.width();
        image.height=item.height();
        item.html(image);
        
item.prop("href", "#shownImage");
		item.click(function(event) {
			image = new Image();
        	image.src = getImageLocalStorage(index);
			shownImage.html(image);

	         item.fancybox({
	                    openEffect  : 'none',
	                    closeEffect : 'none'	                   
	          });
	     });
	     	
     	}
	}

	function setSaveImage(){
		$(document).on('click', '#save_btn', saveImageCanvas);
	}


	function updateCanvasPerson(){
		 html2canvas(person_colum, {
                        onrendered: function (canvas) {                            
                            canvasPerson=canvas;

                             $("#shownBody").html(canvasPerson);
                        }
                    });
	}

 
	function setDownloadImage(){

			download_btn.click(function(event) {
			updateCanvasPerson();
			
               
            download_btn.fancybox({
                    openEffect  : 'none',
                    closeEffect : 'none',
                    afterLoad : function(){   
               		this.inner.prepend('<a class="button" id="save_btn"></a>');                     
                    }
                });
            });

         
	}


function randomBg(){
	bgCount = Math.floor((Math.random()*(maxBgImages+1)));
	loadBg();
}
	function randomPerson(){

		headCount = Math.floor((Math.random()*(maxHeadImages+1)));
		bodyCount = Math.floor((Math.random()*(maxBodyImages+1)));
		footCount = Math.floor((Math.random()*(maxFootImages+1)));

		loadHead();
		loadBody();
		loadFoot();

		generateName();
		
	}

	function generateName(){
		name.innerHTML = '<b>' + names[Math.floor((Math.random()*(names.length)))] + '</b>';
		lastname.innerHTML = '<b>' + lastnames[Math.floor((Math.random()*(lastnames.length)))] + '</b>';
	}
	function changeBg(){
		
		
		if(bgCount<maxBgImages){
				bgCount++;
		}else{
				bgCount=0;
		}
		loadBg();
	}

	function changeHead(e){
		if(e.dir == "left"){
			if(headCount>0){
				headCount--;
			}else{
				headCount=maxHeadImages;
			}
			
		}else{
			if(headCount<maxHeadImages){
				headCount++;
			}else{
				headCount=0;
			}
		}
		
		loadHead();
	}

	function changeBody(e){
		if(e.dir == "left"){
			if(bodyCount>0){
				bodyCount--;
			}else{
				bodyCount=maxBodyImages;
			}
			
		}else{
			if(bodyCount<maxBodyImages){
				bodyCount++;
			}else{
				bodyCount=0;
			}
		}

		loadBody();
		
	}

	function changeFoot(e){
		if(e.dir == "left"){
			if(footCount>0){
				footCount--;
			}else{
				footCount=maxFootImages;
			}
			
		}else{
			if(footCount<maxFootImages){
				footCount++;
			}else{
				footCount=0;
			}
		}

		loadFoot();
		
	}

	function loadBg(){
		var imageName = 'img/bg/fondo_'+bgCount+'.png';
		person_colum.css('background', 'url('+ imageName +') no-repeat');
		
	}

	function loadHead(){
		var imageName = 'img/head/head_'+headCount+'.png';
		person_head.css('background', 'url('+ imageName +') no-repeat');
		generateName();
	}

	function loadBody(){
		var imageName = 'img/body/body_'+bodyCount+'.png';
		person_body.css('background', 'url('+ imageName +') no-repeat');
		generateName();
	}

	function loadFoot(){
		var imageName = 'img/foot/foot_'+footCount+'.png';
		person_foot.css('background', 'url('+ imageName +') no-repeat');
		generateName();
	}

	function fillNames(){
		names.push('Alejandro');
		names.push('Natalia');
		names.push('Fernando');

		lastnames.push('Velez');
		lastnames.push('Arango');
		lastnames.push('Morales');
	}

		function saveImageCanvas() {
			html2canvas(person_colum, {
                        onrendered: function (canvas) { 
                        	canvasPerson=canvas;
                        	saveImageLocal(canvasPerson.toDataURL("image/png"));
                       
                        }
                    });
		}

		function saveImageLocal(url) {

			 
		    if (typeof (Storage) !== "undefined") {
		        localStorage.setItem(nameIndexGallery+ galleryImageIndex, url);
		      
		         galleryImageIndex++;
		         if(galleryImageIndex>galleryMaxImages){
		         	galleryImageIndex=1;
		         }
		         
		    
		    }
		    else {
		        alert("Lo sentimos, no se puede guardar la ruta, su navegador no soporta localStorage");
		    }
		};

		function getImageLocalStorage(index){
			return localStorage.getItem(nameIndexGallery+index);
		}
		function setShareImage(){
        	//alert('entro a setshare');
        	$('#share_btn').click(saveImage);
		}
		

	function saveImage(){
		alert('entro a metodo share');
		$.ajax({
              type: "POST",
              url: "php/main.php",
              data: {
                 cmd: "saveImage",
                 img: canvasPerson.toDataURL("image/png")
              },
              dataType:"json"
            })
            .success(function(e) {                 
              //{"error":false,"datos":{"imagen":["..\/img\/uploads\/1ab2c2370877ccef2ff040e990c49be0.jpg","..\/img\/uploads\/thumbs\/1ab2c2370877ccef2ff040e990c49be0.jpg"]}}                  
              console.log(e.datos.imagen[0]);
              if(!e.error)
              {
                //window.open(base_url+ans.datos.imagen[0].split("../")[1]);
                imagen_participacion = e.datos.imagen[0];
                FB.ui(
				  {
				    method: 'feed',
				    name: 'Compartir',
				    link: 'https://www.andresbedoya.co/cadaver_201401',
				    picture: 'https://www.andresbedoya.co/cadaver_201401/u/thumbs/'+e.datos.imagen[0],
				    caption: 'Compartir imagen',
				    description: 'Amalgama es la aplicación del Módulo de Publicaciones Digitales en colaboración con Ingenieria Informática. Un cadaver exquisito diferente y divertido. Juega, crea tu personaje y comparte.'
				  },
				  function(response) {
				    if (response && response.post_id) {
				      alert('Post was published.');
				    } else {
				      alert('Post was not published.');
				    }
				  }
				);  
                 alert(imagen_participacion);
              }else
              {
                alert("Error al crear tu diseño, intenta nuevamente más tarde.");
              }

              loader.fadeOut();
            })
            .error(function(e){
              /*loader.fadeOut();*/
              alert("Error al crear tu diseño, intenta nuevamente más tarde.");});
        }		

})();
